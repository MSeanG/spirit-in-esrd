require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:sean)
    @other_user = users(:arcane)
  end

  test "should redirect index when not logged in" do
    get users_path
    assert_redirected_to login_url
  end

  test "should get new" do
    get signup_path
    assert_response :success
  end

  test "should redirect edit when not logged in" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch user_path(@user), params: {
      user: {
        first_name:   @user.first_name,
        last_name:    @user.last_name,
        credentials:  @user.credentials,
        state:        @user.state,
        phone_number: @user.phone_number,
        email:        @user.email
      }
    }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

end
