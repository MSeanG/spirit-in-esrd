require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:sean)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: {
      user: {
        first_name:  "",
        last_name: "",
        credentials: "",
        state: "",
        phone_number: "",
        email: "foo@invalid",
        password:              "foo",
        password_confirmation: "bar" 
      }
    }
    assert_template 'users/edit'
  end
  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_url(@user)
    first_name = "Foo"
    last_name = "Bar"
    credentials = "MC"
    state = "HI"
    phone_number = "123-456-7890"
    email = "foo@bar.com"
    patch user_path(@user), params: {
      user: {
        first_name:  first_name,
        last_name: last_name,
        credentials: credentials,
        state: state,
        phone_number: phone_number,
        email: email,
        password:              "",
        password_confirmation: "" 
      }
    }
  end
end
